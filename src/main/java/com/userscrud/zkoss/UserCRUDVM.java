package com.userscrud.zkoss;

import com.userscrud.model.User;
import com.userscrud.service.UserService;
import com.userscrud.utilities.MyLib;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.*;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Window;

import java.io.IOException;

public class UserCRUDVM {
    @Wire("#userCRUD")
    private Window win;

	private UserService userService;

	private User selectedRecord;
	private String recordMode;
	private boolean makeAsReadOnly;

	public User getSelectedRecord() {
		return selectedRecord;
	}

	public void setSelectedRecord(User selectedRecord) {
		this.selectedRecord = selectedRecord;
	}

	public String getRecordMode() {
		return recordMode;
	}

	public void setRecordMode(String recordMode) {
		this.recordMode = recordMode;
	}

	public boolean isMakeAsReadOnly() {
		return makeAsReadOnly;
	}

	public void setMakeAsReadOnly(boolean makeAsReadOnly) {
		this.makeAsReadOnly = makeAsReadOnly;
	}

	@AfterCompose
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("selectedRecord") User user,
			@ExecutionArgParam("recordMode") String recordMode)
			throws IOException {
        userService = (UserService) SpringUtil.getBean("userService");
		Selectors.wireComponents(view, this, false);
		setRecordMode(recordMode);

		if (recordMode.equals("NEW")) {
			this.selectedRecord = new User();
		}
		else if (recordMode.equals("EDIT")) {
			this.selectedRecord = user;
		}
		else if (recordMode == "READ") {
            this.selectedRecord = user;
			setMakeAsReadOnly(true);
		}
	}

	@Command
    public void saveThis(@BindingParam("action") Integer action) {
		userService.save(this.selectedRecord);
		MyLib.showSuccessmessage();
        BindUtils.postGlobalCommand("userQueue", null, "doRefresh",
                null);
		win.detach();
	}

	@Command
	public void cancel() {
		win.detach();
	}
}
