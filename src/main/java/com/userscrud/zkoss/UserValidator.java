package com.userscrud.zkoss;

import java.util.Map;

import org.zkoss.bind.Property;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;

public class UserValidator extends AbstractValidator {

    public void validate(ValidationContext ctx) {
        //all the bean properties
        Map<String,Property> beanProps = ctx.getProperties(ctx.getProperty().getBase());
        //first let's check the passwords match
        validateName(ctx, (String)beanProps.get("name").getValue());
        validateAge(ctx, (Integer)beanProps.get("age").getValue());

    }

    private void validateAge(ValidationContext ctx, int age) {
        if(age <= 0) {
            this.addInvalidMessage(ctx, "age", "Возраст должен быть > 0!");
        }
    }

    private void validateName(ValidationContext ctx, String name) {
        if(name == null || name.trim().equals("")) {
            this.addInvalidMessage(ctx, "name", "Укажите имя!");
        }
    }

}
