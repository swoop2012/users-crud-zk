package com.userscrud.zkoss;

import com.userscrud.model.User;
import com.userscrud.model.UserSearch;
import com.userscrud.service.UserService;
import com.userscrud.utilities.ConfirmResponse;
import com.userscrud.utilities.MyLib;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.*;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Center;
import org.zkoss.zul.Paging;
import org.zkoss.zul.event.PagingEvent;
import java.util.HashMap;
import java.util.List;

public class UserListVM implements ConfirmResponse {

	private Center centerArea;
	private UserSearch userSearch = new UserSearch();

	@Autowired
	private UserService userService;

    @Wire
    private Paging paging;

	private User selectedItem;

    private List<User> currentRecords;

    private long totalItems;

    private int pageSize = 10;

    private int pageNumber = 0;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }


    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public User getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(User selectedItem) {
		this.selectedItem = selectedItem;
	}

	public UserSearch getUserSearch() {
		return userSearch;
	}

	public void setDataFilter(UserSearch userSearch) {
		this.userSearch = userSearch;
	}

    public long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(long totalItems) {
        this.totalItems = totalItems;
    }

	@AfterCompose
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
        userService = (UserService) SpringUtil.getBean("userService");
		Selectors.wireComponents(view, this, false);
        refresh();
    }

    public List<User> getCurrentRecords() {
		return currentRecords;
	}

	@Command
	public void onAddNew() {
		final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("user", null);
        map.put("recordMode", "NEW");
		Executions.createComponents("userCRUD.zul", centerArea, map);
	}

    @Command
	public void onEdit(@BindingParam("userRecord") User user) {

		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("selectedRecord", user);
		map.put("recordMode", "EDIT");
		map.put("centerArea", centerArea);
		Executions.createComponents("userCRUD.zul", centerArea, map);
	}

	@Command
	public void onDelete(@BindingParam("userRecord") User user) {
        selectedItem = user;
		MyLib.confirm("deleteFirstConfirm", "Данный пользователь  \""
				+ user.getName() + "\" будет удалён.?",
				"Confirmation", this);

	}

	@NotifyChange({"currentRecords","paging"})
	@Command
	public void doFilter() {
        setFirstPage();
    }

    @NotifyChange({"currentRecords","paging","userSearch"})
    @Command
    public void clearFilter() {
        userSearch.clear();
        setFirstPage();
    }


    @NotifyChange({"currentRecords","paging"})
    @Command
    public void doPaging(@ContextParam(ContextType.TRIGGER_EVENT) PagingEvent event) {
        pageNumber = event.getActivePage();
        refresh();
    }

    @NotifyChange("currentRecords")
    @GlobalCommand
    public void doRefresh() {
        refresh();
    }

	@Override
	public void onConfirmClick(String code, int button) {
        if(button == 32)
            return;
        if(currentRecords.size() == 1)
            pageNumber--;
        userService.remove(selectedItem);
        refresh();
        BindUtils.postNotifyChange("userQueue", null, UserListVM.this, "currentRecords");
        MyLib.showSuccessmessage();
	}

    private void refresh(){
        Page<User> users = userService.listUsers(userSearch, new PageRequest(pageNumber, pageSize));
        setTotalItems(users.getTotalElements());
        currentRecords = users.getContent();
        paging.setTotalSize((int) totalItems);
    }

    private void setFirstPage() {
        paging.setActivePage(0);
        pageNumber = 0;
        refresh();
    }
}
