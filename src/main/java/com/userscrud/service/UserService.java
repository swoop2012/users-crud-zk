package com.userscrud.service;

import com.userscrud.model.User;
import com.userscrud.model.UserSearch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by User on 22.12.2014.
 */

public interface UserService {
    public void save(User user);
    public Page<User> listUsers(UserSearch user, Pageable pageable);
    public User getUserById(int id);
    public void removeUser(int id);
    public void remove(User user);

}
