package com.userscrud.service;

/**
 * Created by User on 22.12.2014.
 */


import com.userscrud.model.User;
import com.userscrud.model.UserSearch;
import com.userscrud.repository.UserRepository;
import com.userscrud.specification.UserSpecs;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserRepository userRepository;

    @Override
    @Transactional
    public void save(User user) {
        this.userRepository.saveAndFlush(user);
    }

    @Override
    public Page<User> listUsers(UserSearch user, Pageable pageable) {
        return this.userRepository.findAll(UserSpecs.getSearchCriteria(user), pageable);
    }

    @Override
    @Transactional
    public User getUserById(int id) {
        return this.userRepository.findOne(id);
    }

    @Override
    @Transactional
    public void removeUser(int id) {
        this.userRepository.delete(id);
    }

    @Override
    @Transactional
    public void remove(User user) {
        this.userRepository.delete(user);
    }


}