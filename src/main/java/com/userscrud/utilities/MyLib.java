package com.userscrud.utilities;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.ClickEvent;

import javax.validation.ConstraintViolation;
import java.util.*;

public final class MyLib {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void confirm(final String code, final String msg,
			final String title, final ConfirmResponse cnf) {

		Map params = new HashMap();
		params.put("width", 500);

		Messagebox.Button[] messageBoxButtons;
        messageBoxButtons = new Messagebox.Button[] { Messagebox.Button.YES,
				Messagebox.Button.NO };
        Messagebox.setTemplate("/components/messagebox.zul");
		Messagebox.show(msg, "Подтверждение", messageBoxButtons, null,
				Messagebox.QUESTION, null, new EventListener<ClickEvent>() {
					@Override
					public void onEvent(ClickEvent e) throws Exception {
						if (null == cnf) {
							return;
						}
						if (Messagebox.ON_YES.equals(e.getName())) {
							cnf.onConfirmClick(code, Messagebox.YES);
						} else if (Messagebox.ON_NO.equals(e.getName())) {
							cnf.onConfirmClick(code, Messagebox.NO);
						}

					}
				}, params);
	}

	public static void showSuccessmessage() {
		Clients.showNotification("Выполнено успешно",
				Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2100);
	}
}
