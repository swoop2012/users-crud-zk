package com.userscrud.utilities;

public interface ConfirmResponse {
	public void onConfirmClick(String code, int button);
}
