package com.userscrud.repository;

import com.userscrud.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


/**
 * Created by User on 23.12.2014.
 */
@RepositoryRestResource(collectionResourceRel = "user", path = "users-crud")
public interface UserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {
    Page<User> findAll(Specification<User> specification, Pageable pageable);
}
